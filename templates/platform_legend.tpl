%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%# --------------------------------------------------------------------------
%# LEGEND
<div>
  <table>
    <tbody>
    <tr>
      <td class="legend headeritem">Legend&nbsp;&nbsp;</td>
      <td class="legend success"
          title="All tests passed">Passed</td>
      <td class="legend failure"
          title="There is a new failure. Take a look!">Failed</td>
      <td class="legend"
          title="Test not run.">No&nbsp;data</td>
      <td class="legend warning_summary"
          title="Test problem other than FAIL occured: ABORT, ERROR, TEST_NA, WARN.">Other</td>
      <td class="legend experimental"
          title="This is an EXPERIMENTAL test.">Experimental</td>
    </tr>
    </tbody>
  </table>
</div>
<br>
